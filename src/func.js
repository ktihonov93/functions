const getSum = (str1, str2) => {
  // add your implementation below
  if (isFinite(+str1) && isFinite(+str2) && typeof str1 !== 'object' && typeof str2 !== 'object') {
    let sum = +str1 + +str2;
    return sum.toString();
  }
  return false;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let obj = {
    "Post": 0,
    "comments": 0,
    toString() {
      return `Post:${this["Post"]},comments:${this["comments"]}`;
    },
  };

  function findPostsAndComments(list) {
    for (let i in list) {
      if (authorName === list[i].author) {

        if (list[i].post) {
          obj["Post"]++
        } else if (list[i].comment) {
          obj["comments"]++
        }
      }
      if (typeof list[i].comments === 'object') {
        findPostsAndComments(list[i].comments)
      }
    }
  }
  findPostsAndComments(listOfPosts)

  return obj.toString();
}

const tickets = (people) => {
  // add your implementation below
  let boxOffice = { '+100': 0, '+50': 0, '+25': 0 };
  for (let i in people) {
    boxOffice[`+${people[i]}`]++
    let change = people[i] - 25;
      for (let bill in boxOffice) {
        while (boxOffice[bill] > 0 && bill <= change && change > 0) {
          boxOffice[bill]--
          change -= bill;
        }
      }
      if (change > 0) return 'NO';
  }
  return 'YES';
}


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
